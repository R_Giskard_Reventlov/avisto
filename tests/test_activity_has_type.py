from portail_avisto import activity_has_unupdatable_type


def test_has_type():
    acti = {
        "dayParts": [{
            "partInfos": {
                "type": "CP",
                "isAffFor": False,
                "libelle": None,
                "work_real": None
            }
        }, {
            "partInfos": {
                "type": None,
                "isAffFor": False,
                "libelle": None,
                "work_real": None
            }
        }]
    }

    assert activity_has_unupdatable_type(acti) is True


def test_has_not_type():
    acti = {
        "dayParts": [{
            "partInfos": {
                "type": "",
                "isAffFor": False,
                "libelle": None,
                "work_real": None
            }
        }, {
            "partInfos": {
                "type": None,
                "isAffFor": False,
                "libelle": None,
                "work_real": None
            }
        }]
    }

    assert activity_has_unupdatable_type(acti) is False

    acti = {
        "dayParts": [{
            "partInfos": {
                "type": None,
                "isAffFor": False,
                "libelle": None,
                "work_real": None
            }
        }, {
            "partInfos": {
                "type": None,
                "isAffFor": False,
                "libelle": None,
                "work_real": None
            }
        }]
    }

    assert activity_has_unupdatable_type(acti) is False


def test_is_cra():
    acti = {
        "dayParts": [{
            "partInfos": {
                "type": "CRA",
                "isAffFor": False,
                "libelle": None,
                "work_real": None
            }
        }, {
            "partInfos": {
                "type": None,
                "isAffFor": False,
                "libelle": None,
                "work_real": None
            }
        }]
    }

    assert activity_has_unupdatable_type(acti) is False

    acti = {
        "dayParts": [{
            "partInfos": {
                "type": None,
                "isAffFor": False,
                "libelle": None,
                "work_real": None
            }
        }, {
            "partInfos": {
                "type": "CRA",
                "isAffFor": False,
                "libelle": None,
                "work_real": None
            }
        }]
    }

    assert activity_has_unupdatable_type(acti) is False
