import argparse
import calendar
import configparser
import datetime
import getpass
import json
import logging
import colorlog
import os
import sys
import random
import re
import requests

from bs4 import BeautifulSoup
from collections import defaultdict
from delorean import Delorean
from workalendar.europe import France

handler = colorlog.StreamHandler()
handler.setFormatter(colorlog.ColoredFormatter('%(log_color)s%(asctime)s %(levelname)s: %(message)s'))

logger = logging.getLogger(__name__)
logger.addHandler(handler)
logger.setLevel(logging.INFO)

URLS = {
    'SET_ACTIVITY': 'https://portail.advans-group.com/ActivityReport/SetActivity?Length=',
    'GET_ACTIVITY': 'https://portail.advans-group.com/ActivityReport/GetActivity?Length=',
    'CLOSE_ACTIVITY': 'https://portail.advans-group.com/ActivityReport/CloseMonth?Length=',
    'LOGIN': 'https://portail.advans-group.com/Account/Login?ReturnUrl=/ActivityReport',
}


CONTRACTS = {
    "37h30": {
        "hours": 3,
        "minutes": 45
    },
    "38h30": {
        "hours": 3,
        "minutes": 51
    }
}


def check_parameters():
    # Create the parameters parser
    parser = argparse.ArgumentParser(description='Set activity for the given month')
    parser.add_argument('-m', '--month', type=check_positive, help='Month for which the activity needs to be set (ex: 6). Defaults to current month.', default=datetime.date.today().month)
    parser.add_argument('-y', '--year', type=check_positive, help='Year for which the activity needs to be set (ex: 2016). Defaults to current year.', default=datetime.date.today().year)
    parser.add_argument('-d', '--days', type=str, help='Days for which the the activity will be set (ex: 10,11AM,12PM,13)')
    parser.add_argument('-t', '--task', type=str, help='Task to set (ex: "A000000426     03")')
    parser.add_argument('-w', '--work', type=str, help='Work performed (ex: "Automation and monitoring")')
    parser.add_argument('-p', '--password', type=str, help='Password')
    parser.add_argument('-o', '--overwrite', action="store_true", help='Overwrite activities already set. This is implicit if the -d parameter is passed')
    parser.add_argument('-f', '--force', action="store_true", help='Force update of unupdatable activity (eg. holidays, vacation). Implies --overwrite')
    parser.add_argument('-x', '--proxy', type=str, help='Define your proxy url')

    parser.add_argument('--dry-run', action='store_true', help='Avoid writing activities')
    parser.add_argument('--tasklist', dest='tasklist', action='store_true', help='List tasks')
    parser.add_argument('--cloture', dest='cloture', action='store_true', help='Close month activity')
    parser.add_argument('--summary', dest='summary', action='store_true', help='Display a summary of the given month')

    # Parse input parameters
    args = parser.parse_args()
    if args.days or args.force:
        args.overwrite = True
    return parser, args


def check_positive(value):
    ivalue = int(value)
    if not ivalue > 0:
        raise argparse.ArgumentTypeError(f"'{value}' is not a valid positive int value")
    return ivalue


def get_credentials_from_secret():
    script_directory = os.path.dirname(os.path.abspath(__file__))
    secret_file = os.path.join(script_directory, '.secret')
    if os.path.exists(secret_file):
        logger.info("Getting credentials from .secret file")
        config = configparser.ConfigParser()
        config.read(secret_file)
        login = config.get("Credentials", "login")
        password = config.get("Credentials", "password")
        if login is None or login == "" or password is None or password == "":
            logger.warning("Invalid credentials found in .secret file")
            return None

        return {"login": login, "password": password, "contract": config.get("Credentials", "contract", fallback="37h30").lower()}
    else:
        logger.info(".secret file not found")
        return None


def main():
    # Extract arguments
    parser, args = check_parameters()
    month = args.month
    year = args.year
    task = args.task
    work = args.work
    proxy = args.proxy
    json_tasks = []

    # Open login page and retrievers csrf token
    logger.info("Opening session")
    s = requests.session()
    home_page = s.get(URLS["LOGIN"])
    
    if proxy:
        s.proxies = {'http': proxy, 'https': proxy}

    soup = BeautifulSoup(home_page.text, 'html.parser')
    token = soup.select('input[name=__RequestVerificationToken]')[0]['value']

    # Log in
    credentials = get_credentials_from_secret()
    if credentials is None:
        credentials = {}
        credentials["login"] = input('Votre login: ')
        credentials["password"] = getpass.getpass('Votre mot de passe: ')
        credentials["contract"] = input('Votre contrat (37h30 ou 38h30): ').lower()

    if credentials["contract"] != "37h30" and credentials["contract"] != "38h30":
        raise Exception("Invalid contract duration. Should be '37h30' or '38h30'")

    logger.info(f"Logging in as '{credentials['login']}'")

    res = s.post(URLS["LOGIN"], data={
        "month": month,
        "year": year,
        "UserName": credentials["login"],
        "Password": credentials["password"],
        "__RequestVerificationToken": token
    })

    # Extract "Length" attribute (don't know why it's necessary)
    soup = BeautifulSoup(res.text, 'html.parser')
    length = re.search(r'.*\?Length=([0-9]+)$', soup.select('form[action*="GetActivity?"]')[0]['action']).group(1)

    tasks = extract_tasks(res.text)
    if not tasks:
        logger.warn("Could not retrieve tasks from page")

    if args.tasklist:
        display_tasklist(tasks)

    elif args.summary:
        response = s.get(f"{URLS['GET_ACTIVITY']}{length}&month={month - 1}&year={year}&X-Requested-With=XMLHttpRequest&_={random.randint(10000, 99999)}")
        display_summary(response.json())

    elif args.cloture:
        logger.info(f"Closing month: {month}/{year}")
        if not args.dry_run:
            res = s.post(f"{URLS['CLOSE_ACTIVITY']}{length}", data={"month": month, "year": year})
            logger.info("Result: %s" % (res.text))

    elif task is not None:
        if task not in tasks:
            matching_tasks = [code for code, t in tasks.items() if t['Libelle'].lower().find(task) != -1]
            if len(matching_tasks) != 1:
                logger.error(f"Invalid task: {task}")
                display_tasklist(tasks)
                return
            task = matching_tasks[0]

        # Retrieve days with an activity already set
        res = s.get(f"{URLS['GET_ACTIVITY']}{length}&month={month - 1}&year={year}&X-Requested-With=XMLHttpRequest&_={random.randint(10000, 99999)}")

        current_activities = res.json()
        existing_activities = {(acti["year"], acti["month"], acti["day"]): acti for acti in current_activities["data"]}

        # Instantiate calendar objects
        cal = calendar.Calendar()  # used to loop on all days of the month
        workal = France()  # used to filter on working days only

        days_to_update = parse_days(args.days) if args.days else {d: ["AM", "PM"] for d in cal.itermonthdays(year, month) if d > 0}

        for day_to_update, part_days in days_to_update.items():
            current_day = (year, month, day_to_update)
            current_day_label = f"{year}-{month}-{day_to_update}"
            if current_day in existing_activities:
                if activity_has_unupdatable_type(existing_activities[current_day]) and not args.force:
                    logger.info(f"{current_day_label} has unupdatable activity and --force is not set")
                    continue
                if not args.overwrite:
                    logger.info(f"{current_day_label} has activity and --overwrite is not set")
                    continue

            # Calculate yesterday (API takes yesterday as input instead of today...)
            # To add activity to 2016-07-01, date posted must be 2016-06-01T23:00:00.000Z
            day = datetime.datetime(*current_day)
            dl = Delorean(day, timezone='Europe/Paris')

            # Filter on working days only
            if workal.is_working_day(day):
                # Create data to POST
                data = {
                    "date": dl.shift('UTC').datetime.strftime("%Y-%m-%dT%H:%M:%S.000Z")
                }
                add_current_task(current_day, data, existing_activities, credentials["contract"])

                if "AM" in part_days:
                    data["MAT"]["affaire"] = task
                    data["MAT"]["work"] = work or ""

                if "PM" in part_days:
                    data["PAM"]["affaire"] = task
                    data["PAM"]["work"] = work or ""

                logger.info(f"Setting activity for {current_day_label}, payload: {data}")
                if not args.dry_run:
                    res = s.post(f"{URLS['SET_ACTIVITY']}{length}", {'data': str(data)})
                    logger.info(f"Result: '{res.text}'")
            else:
                logger.info(f"{current_day_label} is not a working day")

    else:
        parser.print_help(sys.stderr)
        sys.exit(1)

def extract_tasks(content):
    match = re.search(r'"affaires":(\[{.*?}\])', content)
    if match:
        return {task['Code']: task for task in json.loads(match[1])}
    return None


def display_tasklist(tasks):
    for code, task in tasks.items():
        print(f"{code}: {task['Libelle']}")


def display_summary(data):
    data_by_affaire = defaultdict(int)

    for item in data["data"]:
        print("==========")
        print(f"{item['year']}-{item['month']}-{item['day']}")

        for dayPart in item["dayParts"]:
            partInfos = dayPart["partInfos"]
            if partInfos["affaireLbl"] is not None:
                affaire_key = partInfos["affaireLbl"]
                print(f"{partInfos['libelle']}: {partInfos['affaireLbl']} ({partInfos['hours']}h{partInfos['minutes']}m){', '+partInfos['work_real'] if partInfos['work_real'] else ''}")
            else:
                #TODO: if there is vacation over a weekend, the weekend is marked as vacation and added to the counters, event if it shouldn't
                affaire_key = partInfos["type"]
                print(partInfos["type"])

            data_by_affaire[affaire_key] += 0.5

    print("==================")
    for affaire in data_by_affaire:
        print(f"{affaire} = {data_by_affaire[affaire]}")


def add_current_task(day, data, activities, contract_duration):
    if day in activities:
        morning = activities[day]["dayParts"][0]["partInfos"]
        afternoon = activities[day]["dayParts"][1]["partInfos"]
        data["MAT"] = {
            "affaire": morning["affaire"],
            "tache": "1",
            "work": morning["work_real"],
            "probleme": morning["problems"] or "",
            "linePlan": "0",
            "hours": str(CONTRACTS[contract_duration]["hours"]),
            "minutes": str(CONTRACTS[contract_duration]["minutes"])
        }
        data["PAM"] = {
            "affaire": afternoon["affaire"],
            "tache": "1",
            "work": afternoon["work_real"],
            "probleme": afternoon["problems"] or "",
            "linePlan": "0",
            "hours": str(CONTRACTS[contract_duration]["hours"]),
            "minutes": str(CONTRACTS[contract_duration]["minutes"])
        }
    else:
        data["MAT"] = {
            "affaire": None,
            "tache": "1",
            "work": "",
            "probleme": "",
            "linePlan": "0",
            "hours": str(CONTRACTS[contract_duration]["hours"]),
            "minutes": str(CONTRACTS[contract_duration]["minutes"])
        }
        data["PAM"] = {
            "affaire": None,
            "tache": "1",
            "work": "",
            "probleme": "",
            "linePlan": "0",
            "hours": str(CONTRACTS[contract_duration]["hours"]),
            "minutes": str(CONTRACTS[contract_duration]["minutes"])
        }


def parse_days(days):
    a_days = [d.strip() for d in days.split(",")]
    d_days = {}
    for day in a_days:
        match = re.match(r'^([0-9]+)\s*([A-Z]+)', day, re.IGNORECASE)
        if match:
            if match.group(2).upper() not in ["AM", "PM"]:
                raise ValueError(f"Invalid part of day: {match.group(2).upper()}")
            d_days[int(match.group(1))] = [match.group(2).upper()]
        else:
            d_days[int(day)] = ["AM", "PM"]

    return d_days


def activity_has_unupdatable_type(activity):
    if "dayParts" not in activity:
        return False

    return any(_d["partInfos"]["type"] not in ["CRA", "", None] for _d in activity["dayParts"])


if __name__ == "__main__":
    main()
